<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Upload;

use Illuminate\Contracts\Container\Container;
use Sebwite\Upload\Contracts\Factory as FactoryContract;

/**
 * This is the Factory.
 *
 * @package        Sebwite\Upload
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
class Factory implements FactoryContract
{
    protected $container;

    protected $uploaderClass = Uploader::class;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function make($fields, $request = null)
    {
        $params = compact('fields');
        if ($request !== null) {
            $params += compact('request');
        }
        /**
         * @var Uploader $uploader
         */
        $uploader = $this->container->make($this->uploaderClass, $params);
        return $uploader;
    }

    /**
     * get uploaderClass value
     *
     * @return mixed
     */
    public function getUploaderClass()
    {
        return $this->uploaderClass;
    }

    /**
     * Set the uploaderClass value
     *
     * @param mixed $uploaderClass
     * @return Factory
     */
    public function setUploaderClass($uploaderClass)
    {
        $this->uploaderClass = $uploaderClass;

        return $this;
    }


    /**
     * Set the container value
     *
     * @param \Illuminate\Contracts\Container\Container $container
     * @return Factory
     */
    public function setContainer($container)
    {
        $this->container = $container;

        return $this;
    }
}
