<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Upload;

use Illuminate\Contracts\Support\MessageProvider;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Sebwite\Support\Path;

/**
 * This is the class UploaderResult.
 *
 * @package        Sebwite\Upload
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
class UploaderResult implements MessageProvider
{
    /**
     * @var \Illuminate\Support\Collection
     */
    protected $files;

    /**
     * @var \Illuminate\Support\MessageBag
     */
    protected $messages;

    public function __construct(Collection $files, MessageBag $messages)
    {
        $this->files    = $files;
        $this->messages = $messages;
    }

    /**
     * getFiles
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile[]
     */
    public function getFiles()
    {
        return $this->files->toArray();
    }

    /**
     * getFile
     *
     * @param $field
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getFile($field)
    {
        return $this->files->get($field);
    }

    public function getFileRelativePath($field, $basePath)
    {
        return Path::makeRelative($this->getFile($field)->getPathname(), $basePath);
    }

    /**
     * fails
     *
     * @return bool
     */
    public function fails()
    {
        return ! $this->messages->isEmpty();
    }

    public function count()
    {
        return $this->files->count();
    }

    public function isEmpty()
    {
        return $this->files->isEmpty();
    }

    /**
     * Get the messages for the instance.
     *
     * @return \Illuminate\Contracts\Support\MessageBag
     */
    public function getMessageBag()
    {
        return $this->messages;
    }
}
