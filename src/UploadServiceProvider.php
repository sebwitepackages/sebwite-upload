<?php

namespace Sebwite\Upload;

use Sebwite\Support\ServiceProvider;

/**
 * The main service provider
 *
 * @author        Sebwite
 * @copyright     Copyright (c) 2015, Sebwite
 * @license       https://tldrlegal.com/license/mit-license MIT
 * @package       Sebwite\Upload
 */
class UploadServiceProvider extends ServiceProvider
{
    protected $dir = __DIR__;

    protected $configFiles = [ 'sebwite.upload' ];

    protected $providers = [
        \Cviebrock\ImageValidator\ImageValidatorServiceProvider::class
    ];

    protected $singletons = [
        'sebwite.upload'        => Factory::class
    ];

    protected $aliases = [
        'sebwite.upload' => Contracts\Factory::class
    ];
}
