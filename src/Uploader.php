<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Upload;

use Closure;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Validation\Factory as Validation;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Sebwite\Support\Path;
use Sebwite\Support\Traits\ContainerTrait;

/**
 * This is the Uploader.
 *
 * @package        Sebwite\Platform
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
class Uploader
{
    use ContainerTrait;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $fs;

    /**
     * @var \Illuminate\Contracts\Validation\Validator
     */
    protected $validation;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $rules;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $transformers;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $renamers;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $files;

    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;


    /** Instantiates the class
     *
     * @param \Illuminate\Contracts\Container\Container                                           $container
     * @param \Illuminate\Filesystem\Filesystem                                                   $files
     * @param \Illuminate\Contracts\Validation\Factory|\Illuminate\Contracts\Validation\Validator $validation
     * @param \Illuminate\Http\Request                                                            $request
     * @param                                                                                     $fields
     *
     */
    public function __construct(Container $container, Filesystem $files, Validation $validation, Request $request, $fields)
    {
        $this->setContainer($container);
        $this->fs           = $files;
        $this->validation   = $validation;
        $this->request      = $request;
        $this->rules        = new Collection();
        $this->transformers = new Collection();
        $this->renamers     = new Collection();
        $this->files        = new Collection();
        $this->fields($fields);
    }

    /**
     * fields
     *
     * @param $fields
     * @return $this
     */
    public function fields($fields)
    {
        if (! is_array($fields)) {
            $fields = [ $fields ];
        }

        foreach ($fields as $field) {
            if ($this->request->files->has($field)) {
                $this->files->put($field, $this->request->file($field));
            }
        }

        return $this;
    }

    /**
     * withRules method
     *
     * @param $field
     * @param $rules
     * @return $this
     */
    public function rule($field, $rules)
    {
        $this->rules->put($field, $rules);

        return $this;
    }

    public function rename($field, Closure $renamer = null)
    {
        $this->renamers->put($field, $renamer);

        return $this;
    }

    /**
     * as method
     *
     * @param          $field
     * @param \Closure $transformer
     * @return $this
     */
    public function transform($field, Closure $transformer)
    {
        $this->transformers->put($field, $transformer);

        return $this;
    }

    /**
     * upload method
     *
     * @param      $uploadPath
     * @param bool $dryRun
     * @return \Sebwite\Upload\UploaderResult
     */
    public function upload($uploadPath, $dryRun = false)
    {
        $messages = new MessageBag();
        $valid    = true;

        # Validate
        if (! $this->rules->isEmpty()) {
            $validator = $this->validation->make(
                $this->files->toArray(),
                $this->rules->toArray()
            );
            $messages->merge($validator);
            $valid = $validator->fails();
        }

        if ($valid) {
            $this->ensureDirectory($uploadPath);

            foreach ($this->files->keys() as $field) {
                $file = $this->files->get($field);
                $name = null;

                # Rename
                if ($this->renamers->has($field)) {
                    $renamer = $this->renamers->get($field);

                    if ($renamer === null) {
                        $name = $this->generateUniqueName($file->getClientOriginalName());
                    } else {
                        $name = call_user_func_array($renamer, [ $file->getClientOriginalName() ]);
                    }
                }

                # Move
                if (! $dryRun) {
                    $file = $file->move($uploadPath, $name);
                }

                # Transform
                if ($this->transformers->has($field)) {
                    $transformer = $this->transformers->get($field);
                    $file = $this->getContainer()->call($transformer, compact('file'));
                }


                $this->files->put($field, $file);
            }
        }

        return new UploaderResult($this->files, $messages);
    }

    protected function generateUniqueName($fileName)
    {
        $name = Path::getFilenameWithoutExtension($fileName);
        $ext  = Path::getExtension($fileName);
        $uniq = uniqid($name, false);

        return "{$uniq}.{$ext}";
    }

    protected function ensureDirectory($path)
    {
        $fs = $this->fs;
        if (! $fs->exists($path) || ! $fs->isDirectory($path)) {
            $fs->makeDirectory($path, 0755, true);
        }
    }
}
