<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Upload\Contracts;

/**
 * This is the Factory.
 *
 * @package        Sebwite\Upload
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
interface Factory
{
    /**
     * Creates a new uploader instance
     *
     * @param $fields
     * @param \Illuminate\Http\Request|null $request
     * @return \Sebwite\Upload\Uploader
     */
    public function make($fields, $request);
}
