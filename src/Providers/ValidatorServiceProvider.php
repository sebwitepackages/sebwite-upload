<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Upload\Providers;

use Sebwite\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    protected $rules = array(
        'image_size',
        'image_aspect',
    );


    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../lang', 'image-validator');

        $this->app->bind('Sebwite\Upload\Validators\AudioValidator', function($app)
        {
            $validator = new ImageValidator($app['translator'], [], [], trans('image-validator::validation') );

            if (isset($app['validation.presence']))
            {
                $validator->setPresenceVerifier($app['validation.presence']);
            }

            return $validator;

        });

        $this->addNewRules();
    }


    /**
     * Get the list of new rules being added to the validator.
     * @return array
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Returns the translation string depending on laravel version
     * @return string
     */
    protected function loadTranslator()
    {
        return trans('image-validator::validation');
    }

    /**
     * Add new rules to the validator.
     */
    protected function addNewRules()
    {
        foreach($this->getRules() as $rule)
        {
            $this->extendValidator($rule);
        }
    }


    /**
     * Extend the validator with new rules.
     * @param  string $rule
     * @return void
     */
    protected function extendValidator($rule)
    {
        $method = studly_case($rule);
        $translation = trans('image-validator::validation');
        $this->app['validator']->extend($rule, 'Cviebrock\ImageValidator\ImageValidator@validate' . $method, $translation[$rule]);
        $this->app['validator']->replacer($rule, 'Cviebrock\ImageValidator\ImageValidator@replace' . $method );
    }



}
