Sebwite Upload
====================

[![Build Status](https://img.shields.io/travis/sebwite/upload.svg?&style=flat-square)](https://travis-ci.org/sebwite/upload)
[![Scrutinizer coverage](https://img.shields.io/scrutinizer/coverage/g/sebwite/upload.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/upload)
[![Scrutinizer quality](https://img.shields.io/scrutinizer/g/sebwite/upload.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/upload)
[![Source](http://img.shields.io/badge/source-sebwite/upload-blue.svg?style=flat-square)](https://github.com/sebwite/upload)
[![License](http://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](https://tldrlegal.com/license/mit-license)

Sebwite Upload is a package for the Laravel 5 framework.

The package follows the FIG standards PSR-1, PSR-2, and PSR-4 to ensure a high level of interoperability between shared PHP code.

Documentation
-------------
Tbd

Quick Installation
------------------
Begin by installing the package through Composer.

```bash
composer require sebwite/upload
```

